\chapter[Ricerca]{Ricerca ai fini della progettazione}
\label{Ricerca ai fini della progettazione}
Dopo avere effettuato l'Analisi dei Requisiti (descritta nel capitolo n.\ref{Analisi dei Requisiti}) per l'applicati\-vo ed aver caratterizzato in maniera sufficientemente esauriente le capacit\`a del sistema, il laureando si \`e preparato alla fase di Progettazione studiando ed informandosi sul problema in esame. Lo scopo di questo capitolo \`e elencare le fonti consultate ed illustrare, con un livello di dettaglio adeguato, come le idee espresse abbiano definito la direzione generale del lavoro svolto.

	\section[Algebraic Approach]{An algebraic approach to file synchronization}
	\label{An algebraic approach to file synchronization}
	Si tratta di una pubblicazione scientifica \cite{algebraicApproachRamseyCsirmaz} in cui si cerca di caratterizzare il problema della sincronizzazione da un punto di vista algebrico. \\
	Gli autori di questo articolo definiscono il concetto, centrale per il ragionamento espresso, di "\textit{lista di operazioni}": si tratta dell'elenco di tutti i cambiamenti apportati ad un insieme di file e cartelle di interesse (\textit{file system}) che lo portano da uno stato \textit{S\textsubscript{i}} allo stato \textit{S\textsubscript{i+1}}. Sulla base di questa idea essi sviluppano la seguente formalizzazione:
		\begin{enumerate}
			\item Un file system pu\`o essere portato da uno stato \textit{S\textsubscript{i}} ad uno stato \textit{S\textsubscript{i+1}} tramite l'applicazione di una \textit{lista di operazioni}. Un esempio di lista di operazioni pu\`o essere:
			\begin{enumerate}
				\item Cancellazione del file \textit{x};
				\item Creazione del file \textit{y};
				\item Creazione della cartella \textit{z}.
			\end{enumerate}
			\item Si supponga di dover sincronizzare \textit{n} file system \textit{F\textsubscript{1}}...\textit{F\textsubscript{n}}, tutti derivati da un file system originale \textit{F}. Per ogni \textit{F\textsubscript{i}} si calcola la corrispondente lista di operazioni \textit{S\textsubscript{i}}, tale che: \[F_{i}=S_{i}(F)\]
			\item Si definisce poi: \[S=S_{1} \cup S_{2} ... \cup S_{n}\]
			\item Per ogni file system \textit{F\textsubscript{i}} si calcola poi una lista di operazioni \textit{R\textit{i}}, dipendente sia da \textit{S} che da \textit{S\textsubscript{i}} tale che: \[R_{1}(F_{1})=R_{2}(F_{2})=...=R_{n}(F_{n})=G\]
			Dove \textit{G} corrisponde al nuovo stato di sincronizzazione.
		\end{enumerate}
	In questa pubblicazione viene poi presentata un algebra delle operazioni che permette di ridurre la lista delle operazioni ad una forma \textit{canonica} e viene fornita una dimostrazione matematica della correttezza di tale procedimento. \\
	I concetti esposti in questo documento non sono stati poi utilizzati, ma hanno agevolato il candidato nella comprensione del problema.

	\section{What is a file synchronizer?}
	\label{What is a file synchronizer}
	Questa pubblicazione scientifica \cite{whatIsAFileSynchronizerPierceBalasubramaniam} affronta la questione della sincronizzazione adottando un approccio differente rispetto alla precedente. Vengono infatti definiti due sotto-problemi di pi\`u facile risoluzione e per ciascuno di essi viene caratterizzata una soluzione, dimostrata essere matematicamente corretta, in base ai requisiti che questa dovrebbe avere. I due sotto-problemi sono:
		\begin{enumerate}
			\item \textbf{Update detection}: si tratta di capire quali file sono cambiati durante la transizione di un file system dallo stato \textit{S} allo stato \textit{S\textsubscript{1}}. Sono presentati ed utilizzati i concetti di \mygls{path} del file e \textit{dirtiness attribute}\footnote{Si tratta di una variabile, specifica per file, impostata a \texttt{true} se il file risulta modificato rispetto all'ultimo stato noto, a \texttt{false} altrimenti};
			\item \textbf{Reconciliation}: dati due file system \textit{F\textsubscript{1}} e \textit{I\textsubscript{2}}, si tratta di generare un file system comune \textit{F} seguendo un insieme di regole date.
		\end{enumerate}
	I requisiti formulati sono i seguenti:
		\begin{itemize}
			\item Requisiti relativi al problema \textit{Update detection}:
			\begin{enumerate}
				\item L'attributo \textit{dirty} deve essere \textit{up-closed}: immaginando la struttura di una cartella come un albero, se un nodo \textit{n} risulta essere "sporco", allora lo devono essere anche tutti gli \mygls{ancestor nodes} di \textit{n};
				\item L'attributo \textit{dirty} deve garantire una stima sicura: dato un nodo \textit{n} deve valere: \[\text{!n.dirty} \implies \text{n non risulta modificato}\]
			\end{enumerate}
			\item Requisiti relativi al problema \textit{Reconciliation}: \\
			Dati due path \textit{p} corrispondenti, presenti in due file system differenti \textit{F\textsubscript{1}} ed \textit{F\textsubscript{2}}:
			\begin{enumerate}
				\item Se il contenuto mappato \`e rimasto invariato, non deve essere effettuata alcuna operazione;
				\item Se il contenuto mappato presente in \textit{F\textsubscript{1}} \`e stato modificato, mentre il contenuto presente in \textit{F\textsubscript{2}} \`e rimasto invariato, viene generato un nuovo file system comune \textit{G} tale che:\[G=F_{1}\]
				\item Se il contenuto \`e stato modificato in entrambi i file system, il conflitto va registrato e non deve essere effettuata alcuna operazione.
			\end{enumerate}
		\end{itemize}
	L'articolo analizza inoltre alcuni software di sincronizzazione in commercio, cercando di verificare quanti di essi soddisfano tutti i requisiti esposti.
	Le idee espresse in questo documento sono state di fondamentale importanza per il lavoro: il candidato ha infatti articolato la progettazione del sistema cercando di soddisfare tutti i punti proposti dagli autori, in maniera tale da garantire la validit\`a e la correttezza di quanto sviluppato.

	\section{Differential Synchronization}
	\label{Differential Synchronization}
	In questo documento \cite{differentialSynchronizationFraser} viene presentata una tecnica studiata per effettuare la sincronizzazione del testo in editor collaborativi. Essa \`e stata sviluppata da un dipendente di Google ed utilizzata in \mygls{Google Docs}.
	L'articolo porta esempi interessanti per quanto riguarda la gestione della sincronizzazione in presenza di errori di rete: le idee illustrate non sono state incorporate direttamente nell'applicativo sviluppato, ma sono state ritenute utili e meritevoli di attenzione per eventuali miglioramenti e modifiche future.

	\section{Altre fonti}
	\label{Altre fonti}
	Ai fini della ricerca sono state consultate anche le seguenti fonti minori:
		\begin{center}
			\begin{longtable}{ | P{5cm} | P{3cm} | P{6cm} | }
			\hline
			\textbf{Titolo} & \textbf{Riferimento bibliografico} & \textbf{Descrizione} \\ \hline
			\textit{SyncAny}: The road to beta & \cite{syncanyDonkervlietHegemanHugtenburgSpruit} & Descrizione del funzionamento e delle idee utilizzate dal software di sincronizzazione \textit{SyncAny} \\ \hline
			\textit{Git} for Computer Scientists & \cite{gitVirtanen} & Descrizione del funzionamento e delle idee utilizzate dal famoso \textit{\textbf{V}ersion \textit{C}ontrol \textit{S}ystem} \\ \hline
			Introduction to \mygls{Microsoft Sync Framework} & \cite{MSF} & Descrizione del funzionamento e delle idee utilizzate dal framework sviluppato da Microsoft \\ \hline
			File synchronization algorithms & \cite{fileSynchronizationAlgorithmsHowson} & Documentazione ed argomentazione sulle scelte implementative per un file synchronizer \\ \hline
			Data Synchronization Primer & \cite{dataSynchronizationPrimerMarinucci} & Introduzione al problema della sincronizzazione, presentazione di una possibile soluzione \\ \hline

			Data Synchronization & \cite{dataSynchronizationMcCormack} & Classificazione di algoritmi di sincronizzazione \\ \hline
			How OfflineIMAP works & \cite{howOfflineIMAPWorksYang} & Caratterizzazione di un algoritmo di sincronizzazione da un punto di vista insiemistico \\
			\hline
			\caption{Elenco delle fonti minori consultate}
			\label{tab: elenco fonti minori} \\
			\end{longtable}
		\end{center}
