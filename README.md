# Abstract

My bachelor thesis for Computer Science degree at UniPD, Padua, Italy.
It has been realized using only free software such as [vim](https://www.vim.org/), [Emacs](https://www.gnu.org/software/emacs/), [Pluma](https://mate-desktop.org/), [LaTeX](https://www.latex-project.org/) for text and [draw.io](https://about.draw.io/) and [GNU Image Manipulation Program](https://www.gimp.org/) for images.

# Index

1.  Introduzione

    1.1 Azienda ospitante

    1.2 Progetto di stage

    1.3 Struttura del documento

    1.4 Convenzioni adottate

    1.5 Informazioni aggiuntive

2. Analisi dei Requisiti

    2.1 Benchmark

    2.2 Questionario

    2.3 Visione generale

    2.4 Possibili punti critici

    2.5 Casi d'uso

    2.6 Requisiti

3. Ricerca

    3.1 Algebraic approach to file synchronization

    3.2 What is a file synchronizer

    3.3 Differential Synchronization

    3.4 Altre fonti

4. Progettazione

    4.1 Sessione di sincronizzazione

    4.2 Algoritmo
    
    4.3 Architettura

5. Sviluppo

    5.1 Algoritmi sviluppati

    5.2 Possibili scelte implementative

    5.3 Possibili tecnologie da impiegare

6. Conclusioni

    6.1 Risultato ottenuto

    6.2 Valutazione prodotto

    6.3 Valutazione stage


A. Questionario

B. Casi d'uso secondari

Glossario

Bibliografia
