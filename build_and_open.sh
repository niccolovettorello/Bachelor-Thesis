#!/bin/sh

# Decommenta le linee seguenti se vuoi ricompilare anche il frontespizio della tesi

#cd cover
#pdflatex cover.tex
#cd ..

pdflatex main.tex

# Compila la bibliografia
biber main.bcf

# Compila il glossario
makeglossaries main

pdflatex main.tex
pdflatex main.tex

evince main.pdf
